function beeswarm(svg, data, valueFn, color, log, reversed, hoverFn){

  var margin = {top: 0, right: 40, bottom: 50, left: 40},
  width = svg.attr("width") - margin.left - margin.right,
  height = svg.attr("height") - margin.top - margin.bottom,
  radius = 4;

  data = data.filter(function(d){ return valueFn(d) !== undefined });

  var x = (log ? d3.scaleLog() : d3.scaleLinear())
      .rangeRound(reversed ? [width, 0]: [0, width])
      .domain(log ? d3.extent(data, function(d) { return valueFn(d); }):
         [0, d3.max(data, function(d) { return valueFn(d); })]);
  
  var g = svg.append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  hoverFn = hoverFn || valueFn;

  var simulation = d3.forceSimulation(data)
      .force("x", d3.forceX(function(d) { return x(valueFn(d)); }).strength(1))
      .force("y", d3.forceY(height / 2))
      .force("collide", d3.forceCollide(radius+1))
      .stop();
  
  for (var i = 0; i < 120; ++i) simulation.tick();
 
  g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x).ticks(10, ".3g"));
  
  var cell = g.append("g")
      .attr("class", "cells")
    .selectAll("g").data(d3.voronoi()
        .extent([[-margin.left, -margin.top], [width + margin.right, height + margin.top]])
        .x(function(d) { return d.x; })
        .y(function(d) { return d.y; })
      .polygons(data)).enter().append("g");
  
  cell.append("circle")
      .attr("r", radius)
      .attr("cx", function(d) { return d.data.x; })
      .attr("cy", function(d) { return d.data.y; })
      .attr("fill", function(d){ 
        if(d.data.series == ""){
          return "#A0A0A0";
        } else {
          return color(d.data.series)
        }
      });
 
  cell.append("path")
      .attr("d", function(d) { return "M" + d.join("L") + "Z"; });
  
  cell.append("title")
      .text(function(d) { return d.data.title + "\n" + hoverFn(d.data) });
}
