var parseTime = function(){ 
 var parseDate = d3.timeParse("%Y-%m-%d");
 var parseMonth = d3.timeParse("%Y-%m");
 var parseYear = d3.timeParse("%Y");
  return function(t){
    if(t.length == 4){
      return parseYear(t)
    }
    if(t.length == 7){
      return parseMonth(t)
    }
    return parseDate(t);
  }
}();

function load_datum(d){ 
  d.age = (Date.now()-parseTime(d.date_published))/ 31536000000.0;
  d.series_details = d.series_details.replace(/\(.*\)/, "").split("|"); 
  d.genres = d.genre.split("/").map(function(g){ 
     return g.replace(/^\s+|\s+$/g, '');
  });
  return d;
};

window.onload = function(){
  var body = d3.select("body");
  
  d3.csv("export.csv", load_datum, function(error, data) {
    if (error) throw error;

    var series = [].concat(
      data.map(function(d){ return d.series_details })
    ).map(function(d){ return d[0] });
 
    data = data.map( function(d){
      d.series = d.series_details.filter( function(s){
        return series.filter(function(ss){return ss==s}).length > 1;
      })[0] || "";
      return d;
    });
 
    var series_color = d3.scaleOrdinal(d3.schemeCategory20);

    window.console.log(data);
    [{ 
       "fn": function(d){
         return d.age;
       },
       "hoverFn": function(d){
         return d.date_published;
       },
       "reversed": true,
       "log": true,
       "title": "Publish Date"
     }, {
       "fn": function(d){
         if(d.list_price !== ""){
           return parseFloat(d.list_price);
         }
       },
      "title": "List Price"
    }, {
       "fn": function(d){
         if(d.pages !== ""){
           return parseInt(d.pages);
         }
       },
      "title": "Pages"
    }].forEach( function(g){
      body.insert("h4").text(g.title);
      beeswarm(
        body.insert("svg")
          .attr("width", 960)
          .attr("height", 150),
        data,
        g.fn, 
        series_color,
        g.log,
        g.reversed,
        g.hoverFn
      ); 
    });

    body.insert("h4").text("Price vs Length");
    scatter(body.insert("svg")
             .attr("width", 500)
             .attr("height", 400),
      data,
      function(d){ 
         if(d.pages !== ""){
           return parseInt(d.pages);
         }
      }, function(d){
         if(d.list_price !== ""){
           return parseFloat(d.list_price);
         }
      }, series_color,
      ["Pages", "List Price"]
     );

    body.insert("h4").text("Series Titles");
    seriesPlot(body.insert("div"), data, series_color);

    [{
      "title": "Formats",
      "histogram": histogram(data.map(function(d){return d.format}))
     }, {
      "title": "Language",
      "histogram": histogram(data.map(function(d){return d.language}))
    }, {
      "title": "Publisher",
      "histogram": histogram(data.map(function(d){return d.publisher})),
      "hideLabel": true
    }].forEach( function(g){
      var sec = body.append("div").classed("pieContainer", true);
      sec.insert("h4").text(g.title);
      pie(
        sec.insert("svg")
          .attr("width", 400)
          .attr("height", 400),
        g.histogram,
        g.hideLabel
      )
    });
    var sec = body.append("div").classed("pieContainer", true);
    sec.insert("h4").text("Genres");

    nested_pie(
      sec.insert("svg")
        .attr("width", 500)
        .attr("height", 500),
      treeify(
        data
         .map(function(d){ return d.genres })
         .filter(function(d){ return d[0] != "" })
    ))
  });
}
