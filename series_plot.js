function uniq(arr){
 return arr.sort().reduceRight( function(acc, d){
    if( acc[0] !== d ){
      return [d].concat(acc)
    }
    return acc;
  }, []);
};

function seriesPlot(div, data, color){
  var series = uniq(data.map(function(d){
    return d.series
  }).filter(function(d){
    return d != "";
  }));
  div = div.append("div");
  div.classed("seriesPlot", true);
  var seriesDiv = div.selectAll(".series")
    .data(series)
    .enter() 
    .append("div")
    .classed("series", true);
  seriesDiv.append("span")
    .style("color", function(d){ return color(d) })
    .classed("bullet", true)
    .text("\u25CF");
  seriesDiv.append("span")
    .text(function(d){return d});
}
