function scatter(svg, data, xFn, yFn, color, titles){
  var margin = {top: 50, right: 50, bottom: 50, left: 50},
  width = svg.attr("width") - margin.left - margin.right,
  height = svg.attr("height") - margin.top - margin.bottom,
  radius = 4;
  data = data.filter(function(d){ return (xFn(d) !== undefined) && (yFn(d) !== undefined); });
  var x = d3.scaleLinear()
      .rangeRound([0, width])
      .domain([0, d3.max(data, function(d) { return xFn(d); })]);
  var y = d3.scaleLinear()
      .rangeRound([height, 0])
      .domain([0, d3.max(data, function(d) { return yFn(d); })]);

  var xBee = data.map(function(d){ return { "data": d } });

  var xSimulation = d3.forceSimulation(xBee)
      .force("x", d3.forceX(function(d) { return x(xFn(d.data)); }).strength(1))
      .force("y", d3.forceY(0))
      .force("collide", d3.forceCollide(radius+1))
      .stop();

  for (var i = 0; i < 120; ++i){
    xSimulation.tick();
    xBee.forEach(function(d){ d.y = d3.max([0, d.y]); });
  }

  var xVoronoi = d3.voronoi()
    .extent([[-margin.left, -margin.top], [width, 0]])
        .x(function(d) { return d.x; })
        .y(function(d) { return -d.y-radius*2; })
      .polygons(xBee);

  var yBee = data.map(function(d){ return { "data": d } });
  var ySimulation = d3.forceSimulation(yBee)
      .force("x", d3.forceX(0))
      .force("y", d3.forceY(function(d) { return y(yFn(d.data)); }).strength(1))
      .force("collide", d3.forceCollide(radius+1))
      .stop();

  for (var i = 0; i < 120; ++i){
    ySimulation.tick();
    yBee.forEach(function(d){d.x = d3.max([0, d.x]);})
  }
  var yVoronoi = d3.voronoi()
    .extent([[width, 0], [width + margin.right, height+margin.top]])
        .x(function(d) { return width + d.x + radius * 2; })
        .y(function(d) { return d.y; })
      .polygons(yBee);

  var g = svg.append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x).ticks(10, ".3g"));

  g.append("g")
      .attr("class", "axis axis--y")
      .call(d3.axisLeft(y).ticks(10, ".3g"));

  g.append("text")
    .attr("class", "x label")
    .attr("text-anchor", "end")
    .attr("x", width/2)
    .attr("y", height+36)
    .text(titles[0]);

  g.append("text")
    .attr("class", "y label")
    .attr("text-anchor", "end")
    .attr("transform", "rotate(-90)")
    .attr("x", -height/2)
    .attr("y", -36)
    .text(titles[1]);

  var voronoi = d3.voronoi()
        .extent([[-margin.left, 0], [width, height + margin.top]])
        .x(function(d) { return x(xFn(d)); })
        .y(function(d) { return y(yFn(d)); })
      .polygons(data);

  var cell = g.append("g")
      .attr("class", "cells")
    .selectAll("g").data(
       d3.zip(voronoi, xVoronoi, yVoronoi)
    ).enter().filter(function(d){return d[0] && d[1] && d[2]}).append("g");
  
    cell.append("circle")
      .attr("r", radius)
      .attr("cx", function(d) { return x(xFn(d[0].data)); })
      .attr("cy", function(d) { return y(yFn(d[0].data)); })
      .attr("fill", function(d) { 
        if( d[0].data.series == ""){
          return "#A0A0A0";
        } else {
          return color(d[0].data.series)
        }
      });

    cell.append("circle")
      .attr("r", radius)
      .attr("cx", function(d) { return d[1].data.x; })
      .attr("cy", function(d) { return -(d[1].data.y+radius*2); })
      .attr("fill", function(d) { 
        if( d[1].data.data.series == ""){
          return "#A0A0A0";
        } else {
          return color(d[1].data.data.series)
        }
      });

    cell.append("circle")
      .attr("r", radius)
      .attr("cx", function(d) { return width + d[2].data.x + radius * 2; })
      .attr("cy", function(d) { return d[2].data.y; })
      .attr("fill", function(d) { 
        if( d[1].data.data.series == ""){
          return "#A0A0A0";
        } else {
          return color(d[1].data.data.series)
        }
      });

  cell.append("path")
      .attr("d", function(d) { return "M" + d[0].join("L") + "Z"; });

  cell.append("path")
      .attr("d", function(d) { return "M" + d[1].join("L") + "Z"; });

  cell.append("path")
      .attr("d", function(d) { return "M" + d[2].join("L") + "Z"; });

  cell.append("title")
      .text(function(d) { return d[0].data.title });
 
  g.append("line")
    .attr("class", "scatter-sep")
    .attr("x1", 0).attr("y1", - radius) 
    .attr("x2", width).attr("y2", - radius);

  g.append("line")
    .attr("class", "scatter-sep")
    .attr("x1", width+radius).attr("y1", 0) 
    .attr("x2", width+radius).attr("y2", height);
}
