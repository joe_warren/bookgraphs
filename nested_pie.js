function treeify(data){
  var topElems = uniq(data.map(d=>d[0]))
  return topElems.map(function(d){
    return {
      "name": d,
      "count": data.filter(e=> e[0]==d).length,
      "children": treeify(
        data.filter(e=> e[0]==d)
        .map(e => e.slice(1))
      ) 
    };
  })
}

function depth(data){
  return data.map(d=>depth(d.children)+1).reduce((a, b)=> Math.max(a,b), 0)
}

function nested_pie(svg, data){
  var total = data.reduce((a, b) => a+b.count, 0);
  var nesting = depth(data)
  var root = d3.hierarchy({
    "name": "root",
    "children": data
  });
  var margin = {top: 10, right: 20, bottom: 20, left: 20},
  width = svg.attr("width") - margin.left - margin.right,
  height = svg.attr("height") - margin.top - margin.bottom,
  radius = Math.min(width, height) / 2,
  inner_radius = radius - 120,
  outer_radius = radius - 20,
  selected_growth = 20,
  color =  d3.scaleOrdinal(d3.schemeCategory20);

  var arc = d3.arc()
 
  root.sum(d=>d.count);
  var partition = d3.partition()
    .size([Math.PI*2,(nesting+1)*(outer_radius-inner_radius)/nesting])
    .padding(0)
    .round(false);
  partition(root);

  svg = svg
    .classed("pie", true)
    .append("g")
    .attr("transform", "translate(" + (width / 2 + margin.left) + "," 
       + (height / 2 + margin.top) +  ")");

  var offset = inner_radius - (outer_radius-inner_radius)/nesting;

  window.console.log(root);

  function nestedColor(d, i){
    if(d.parent.data.name != "root"){
      return nestedColor(d.parent, i).darker((i%5-2.5)/2);
    }
    return d3.color(color(d.data.name));
  }
  function qualifiedName(d){
    if(d.parent.data.name != "root"){
      return qualifiedName(d.parent) + "\u2022" + d.data.name;
    }
    return d.data.name;
  }

  var paths = svg.selectAll(".arc")
      .data(root.descendants())
    .enter().append("g")
      .filter(function(d){ return d.data.name != "root"})
      .attr("class", "arc")
    .append("path")
      .attr("d", function(d){ 
        return arc
          .outerRadius(offset+d.y1)
          .innerRadius(offset+d.y0)
          .startAngle(d.x0)
          .endAngle(d.x1)();
      })
      .style("fill", nestedColor )
      .on("mouseover", function(d, i){
          d3.select(this)
          .transition()
          .attr("d", d3.arc()
            .outerRadius(offset+d.y1+selected_growth)
            .innerRadius(offset+d.y0)
            .startAngle(d.x0)
            .endAngle(d.x1)()
          );
          paths.filter(function(e){
              return e != d && d.descendants().includes(e)
            })
            .transition()
            .attr("d", function(e){
              return d3.arc()
                .outerRadius(offset+e.y1+selected_growth)
                .innerRadius(offset+e.y0+selected_growth)
                .startAngle(e.x0)
                .endAngle(e.x1)();
            });
            center.transition()
              .attr("opacity", 1.0);
            percentage.text(percentFormat(d.data.count/total));
            item.text(qualifiedName(d));
        })
        .on("mouseout", function(d, i) {
          d3.select(this)
          .transition()
          .attr("d", d3.arc()
            .outerRadius(offset+d.y1)
            .innerRadius(offset+d.y0)
            .startAngle(d.x0)
            .endAngle(d.x1)()
          );
          paths.filter(function(e){
              return e != d && d.descendants().includes(e)
            })
            .transition()
            .attr("d", function(e){
              return d3.arc()
                .outerRadius(offset+e.y1)
                .innerRadius(offset+e.y0)
                .startAngle(e.x0)
                .endAngle(e.x1)()
          })
          center.transition()
            .attr("opacity", 0.0);
      });
     var center = svg.append("g")
       .attr("opacity", 0);
     var percentage = center.append("text")
       .classed("percentage", true);
     var item = center.append("text")
       .classed("item", true);
     var percentFormat = d3.format(".0%");
}
