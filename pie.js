function histogram(values){
  var items = uniq(values);
  return items.map( function (i){
    return {
      "value": i,
      "frequency": values.filter(function(d){ return d == i }).length/
        values.length
    }
  });
}

function pie(svg, hist, hideLabel){
  var margin = {top: 10, right: 20, bottom: 20, left: 20},
  width = svg.attr("width") - margin.left - margin.right,
  height = svg.attr("height") - margin.top - margin.bottom,
  radius = Math.min(width, height) / 2,
  inner_radius = radius - 80,
  outer_radius = radius - 20,
  selected_radius = radius,
  color =  d3.scaleOrdinal(d3.schemeCategory20);

  var arc = d3.arc()
    .outerRadius(outer_radius)
    .innerRadius(inner_radius);

  var pie = d3.pie()
    .sort(null)
    .value(function(d) { return d.frequency; });

  svg = svg
    .classed("pie", true)
    .append("g")
    .attr("transform", "translate(" + (width / 2 + margin.left) + "," 
       + (height / 2 + margin.top) +  ")");

  var pieLayout = pie(hist);

  svg.selectAll(".arc")
      .data(pieLayout)
    .enter().append("g")
      .attr("class", "arc")
    .append("path")
      .attr("d", arc)
      .style("fill", function(d) { return color(d.data.value); })
      .on("mouseover", function(d, i) {
          d3.select(this)
          .transition()
          .attr("d", d3.arc()
             .innerRadius(inner_radius)
             .outerRadius(selected_radius));
           center.transition()
            .attr("opacity", 1.0);
           percentage.text(percentFormat(d.data.frequency));
           item.text(d.data.value);
           
      })
      .on("mouseout", function(d, i) {
          d3.select(this)
          .transition()
          .attr("d", d3.arc()
             .innerRadius(inner_radius)
             .outerRadius(outer_radius));
           center.transition()
            .attr("opacity", 0.0);
      });

  var center = svg.append("g")
    .attr("opacity", 0);
  var percentage = center.append("text")
    .classed("percentage", true);
  var item = center.append("text")
    .classed("item", true);
  var percentFormat = d3.format(".0%");

  if( !hideLabel ){
    svg.selectAll(".arcLabel")
      .data(pieLayout)
      .enter().append("g")
      .attr("class", "arcLabel")
      .append("text")
      .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
      .attr("dy", ".35em")
      .text(function(d) { return d.data.value; });
  }
}
